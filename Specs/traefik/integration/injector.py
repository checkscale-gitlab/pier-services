from config import FacilitiesWrapper
import logging

logger = logging.getLogger("log")
class Injector:
    def __init__(self):
        self.name = "traefik"
        self.path = ""
    
    def handle_spec(self, facilities: FacilitiesWrapper, service_name, spec, own_config = None):
        logger.debug("Injecting Traefik for %s", service_name)
        logger.increaseIndent()
        handlers = {
            "middleware": self.generate_middleware,
            "listens_on_port": self.generate_listens_on_port,
            "basepath": lambda _,__,___: None,
        }
        self.path = service_name
        if isinstance(spec, dict) and "basepath" in spec and isinstance(spec["basepath"], str):
            logger.debug("Custom basepath specified")
            self.path = spec["basepath"].strip("/")
        self.inject_base_configuration(facilities, service_name, own_config)
        if isinstance(spec, dict):
            for (key, value) in spec.items():
                handle = handlers.get(key, lambda _,__,___ : logger.warning('Unsupported traefik key: %s', key))
                handle(facilities, service_name, value)
        logger.decreaseIndent()

    def inject_base_configuration(self, facilities: FacilitiesWrapper, service_name, own_config):
        if "https" in own_config and own_config["https"]:
            self.inject_tls_config(facilities, service_name)
        else:
            self.inject_ins_config(facilities, service_name)
        traefik_compose = {
            "services":{
                service_name:{
                    "networks": { 
                        "traefik-network": {}
                    },
                    "labels": {
                        # enables traefik support for the service
                        "traefik.enable": "true"
                        # tells traefik to use the special network for forwarding to  the service
                        , "traefik.docker.network": "traefik-network"
                        # assign service to router
                        , "traefik.http.routers." + service_name + ".service": service_name
                    }
                }
            },
            "networks":{ 
                "traefik-network": {
                    "name": "traefik-network", 
                    "external": True
                }
            }
        }
        facilities.inject_config(traefik_compose)


    def inject_ins_config(self, facilities: FacilitiesWrapper, service_name):
        traefik_compose = {
                "services":{
                    service_name:{
                        "labels":{
                            # ====== HTTP =====
                            # Accepts requests coming on WEB (80)
                            "traefik.http.routers." + service_name + ".entrypoints": "web",
                            # Rule to only accept http to service_name (/service_name)
                            "traefik.http.routers." + service_name + ".rule": "PathPrefix(`/" + self.path + "/`) || Path(`/" + self.path + "`)"
                        }
                    }
                }
            }
        
        facilities.inject_config(traefik_compose)

    def inject_tls_config(self, facilities: FacilitiesWrapper, service_name):
        traefik_compose = {
            "services":{
                service_name:{
                    "labels":{
                        # ====== HTTPS =====
                        # Accepts requests coming on WEBSECURE (443)
                        "traefik.http.routers." + service_name + ".entrypoints": "websecure",
                        # Use TLS
                        "traefik.http.routers." + service_name + ".tls": "true",
                        "traefik.http.routers." + service_name + ".tls.certresolver": "letsencrypt",
                        # Rule to only accept http to service_name ($DOMAIN/service_name)
                        "traefik.http.routers." + service_name + ".rule": "Host(`${DOMAIN}`) && (PathPrefix(`/" + self.path + "/`) || Path(`/" + self.path + "`))"
                    }
                }
            }
        }
        facilities.inject_config(traefik_compose)

    def generate_listens_on_port(self, facilities: FacilitiesWrapper, service_name, port_number):
        traefik_compose = {
            "services":{
                service_name:{
                    "labels":{
                        "traefik.http.services." + service_name + ".loadbalancer.server.port": port_number,
                    }
                }
            }
        }
        facilities.inject_config(traefik_compose)

    def generate_middleware(self, facilities: FacilitiesWrapper, service_name, config):
        # adds support for multiple definitions of middleware by initializing the id with the len of defined middlewares
        path = ["services", service_name, "labels", "traefik.http.routers." + service_name + ".middlewares"]
        existing_middlewares = facilities.get_current_compose_value(path)
        if isinstance(existing_middlewares, str):
            middlewares = existing_middlewares.split(', ')
        else:
            middlewares = []
        offset = len(middlewares)

        for index, middleware_obj in enumerate(config):
            middleware_id = service_name + str(index + offset)
            if "add-trailing-slash" in middleware_obj:
                traefik_compose = {
                    "services":{
                        service_name:{
                            "labels":{
                                "traefik.http.middlewares." + middleware_id + ".redirectregex.regex": "^(.*)/" + self.path + "$$",
                                "traefik.http.middlewares." + middleware_id + ".redirectregex.replacement": "$${1}/" + self.path + "/"
                            }
                        }
                    }
                }
                facilities.inject_config(traefik_compose)
                middlewares.append(middleware_id + "@docker")
            elif "strip-prefix" in middleware_obj:
                traefik_compose = {
                    "services":{
                        service_name:{
                            "labels":{
                                "traefik.http.middlewares." + middleware_id + ".stripprefix.prefixes": "/" + self.path + ",/" + self.path + "/",
                            }
                        }
                    }
                }
                facilities.inject_config(traefik_compose)
                middlewares.append(middleware_id + "@docker")
            elif "replace" in middleware_obj and "from" in middleware_obj["replace"] and "to" in middleware_obj["replace"] :
                traefik_compose = {
                    "services":{
                        service_name:{
                            "labels":{
                                "traefik.http.middlewares." + middleware_id + ".replacepathregex.regex": "^/" + middleware_obj["replace"]["from"] + "(/(.*))?",
                                "traefik.http.middlewares." + middleware_id + ".replacepathregex.replacement":"/" + middleware_obj["replace"]["to"] + "/$$2"
                            }
                        }
                    }
                }
                facilities.inject_config(traefik_compose)
                middlewares.append(middleware_id + "@docker")
            else:
                logger.error("Invalid middleware spec: %s", middleware_obj)


        if len(middlewares) > 0:
            traefik_compose = {
                    "services":{
                        service_name:{
                            "labels":{
                                "traefik.http.routers." + service_name + ".middlewares": ", ".join(middlewares)
                            }
                        }
                    }
            }
            facilities.inject_config(traefik_compose)

    