import random
import string
import bcrypt
from os import path
from utils import install_file_with_content
from exit_codes import ExitCode
import logging

logger = logging.getLogger("log")

class passwordManager:
    def __init__(self, secret_path, env_file_path):
        self.base_path = secret_path
        self.init_file_name = env_file_path

    def expose_secret(self, id, hashtype, generate_key=False):
        secret = self.get_secret_with_id(id, generate_key)

        hashfunctions = {
            "no" : (lambda x: x),
            "bcrypt" : (lambda x: bcrypt.hashpw(secret.encode('utf_8'), bcrypt.gensalt()).decode('utf_8'))
        }

        if hashtype in hashfunctions:
            hashed_secret = hashfunctions[hashtype](secret)
        else:
            logger.critical("Unsupported hash type %s", hashtype)
            exit(ExitCode.UNSUPPORTED_HASH_TYPE)

        file = open(self.init_file_name, "a")
        file.write(id + "=" + hashed_secret + "\n")
        file.close()

    def get_secret_with_id(self, id, generate_key = False):
        if not path.isfile(self.base_path + '/' + id):
            secret = self.get_rand_key() if generate_key else self.get_rand_pass()
            install_file_with_content(self.base_path, id, secret, 0o600)

        file = open(self.base_path + '/' + id, "r")
        secret = file.read()
        file.close()
        return secret
    
    def get_rand_pass(self):
        return self.gen_secret(string.ascii_letters + string.digits, 16)

    def get_rand_key(self):
        return self.gen_secret(string.ascii_letters + string.digits + string.punctuation, 32)

    def gen_secret(self, alphabet, length):
        return ''.join((random.choice(alphabet) for i in range(length)))
