import logging

class IndentLogger(logging.Logger):

    indent = 0
    prefixStr = ""

    def _log(self, level, msg, args, exc_info=None, extra=None):
        indentedMsg = self.getLogPrefix() + msg
        super(IndentLogger, self)._log(level, indentedMsg, args, exc_info, extra)

    def increaseIndent(self):
        type(self).indent += 1

    def decreaseIndent(self):
        type(self).indent -= 1

    def setPrefixStr(self, prefix):
        type(self).prefixStr = prefix
    
    def getLogPrefix(self):
        return type(self).prefixStr + (type(self).indent * "\t")
